#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import codecs
from setuptools import (setup, find_packages)


this = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(this, 'pytest_portaltest', '__version__.py')) as f:
    exec(f.read(), about)


def read(fname):
    file_path = os.path.join(this, fname)
    return codecs.open(file_path, encoding='utf-8').read()


setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    author=about['__author__'],
    author_email=about['__author_email__'],
    maintainer=about['__author__'],
    maintainer_email=about['__author_email__'],
    license=about['__license__'],
    url=about['__url__'],
    packages=find_packages(exclude=['tests']),
    long_description=read('README.rst'),
    python_requires='!=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*',
    install_requires=[
        'pytest>=3.6.0',
        'selenium>=3.5.0',
        'python-dateutil',
        'environs',
        'requests'
    ],
    include_package_data=True,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Framework :: Pytest',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Testing',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
    ],
    entry_points={
        'pytest11': [
            'pytest_portaltest = pytest_portaltest',
        ],
    },
)
