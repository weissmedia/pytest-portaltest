# -*- coding: utf-8 -*-

from pytest_portaltest.utils import _env


def addoption(parser, option, **kwargs):
    parser.addini(kwargs['dest'], default=kwargs['default'], help='default value for {}'.format(option))
    parser.addoption(option, **kwargs)


def pytest_addoption(parser):
    """parser stores browser, orientation etc. for pytest request, needed to run test"""
    bs_server = 'https://{}:{}@hub-cloud.browserstack.com/wd/hub'.format(
        _env('BROWSERSTACK_USERNAME'), _env('BROWSERSTACK_ACCESS_KEY'))
    bs_server = _env('BROWSERSTACK_SERVER', bs_server)
    bs_localrun = _env.bool('BROWSERSTACK_LOCALRUN', False)
    bs_localident = _env('BROWSERSTACK_LOCALIDENT', None)
    addoption(parser, '--browserstack-server', dest='bs_server', action='store', default=bs_server)
    addoption(parser, '--browserstack-local', dest='bs_localrun', action='store_true', default=bs_localrun)
    addoption(parser, '--browserstack-localident', dest='bs_localident', action='store', default=bs_localident)
    addoption(parser, '--test', dest='test', action='store', default=None)
    addoption(parser, '--env', dest='env', action='store', default='test')
    addoption(parser, '--browser', dest='browser', action='store', default='ff_win10')
    addoption(parser, '--orient', dest='orient', action='store', default=None)
    addoption(parser, '--project', dest='project', action='store', default=None)
