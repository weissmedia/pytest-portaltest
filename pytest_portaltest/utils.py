# -*- coding: utf-8 -*-
import functools
from environs import Env
from collections import namedtuple
from enum import (Flag, auto, Enum)
from datetime import datetime
from dateutil import tz


class DeviceOrientation(Enum):
    LANDSCAPE = 'H'

    @classmethod
    def _missing_(cls, value):
        return None


class BrowserGroup(Flag):
    # groups for all browsers and devices in conftest, everything has to be in a group
    FF_WIN10 = auto()
    FF_WIN7 = auto()
    FF56_WIN7 = auto()
    CH_WIN10 = auto()
    CH_WIN7 = auto()
    ED_WIN10 = auto()
    IE_WIN7 = auto()
    IPHONE8 = auto()
    IPHONESE = auto()
    SAMSUNG7 = auto()
    SAMSUNG8 = auto()
    IPAD5 = auto()
    IPADPRO = auto()
    GROUP_FIREFOX = FF_WIN10 | FF_WIN7 | FF56_WIN7
    GROUP_CHROME = CH_WIN10 | CH_WIN7
    GROUP_IE11 = IE_WIN7
    GROUP_EDGE = ED_WIN10
    GROUP_MICROSOFT = ED_WIN10 | IE_WIN7
    GROUP_CHROME_AND_MICROSOFT = GROUP_CHROME | GROUP_MICROSOFT
    GROUP_DESKTOP = GROUP_FIREFOX | GROUP_CHROME | GROUP_MICROSOFT
    GROUP_MOB_AND = SAMSUNG7 | SAMSUNG8
    GROUP_MOB_IOS = IPHONE8 | IPHONESE
    GROUP_MOBILE = GROUP_MOB_AND | GROUP_MOB_IOS
    GROUP_TAB_IOS = IPAD5 | IPADPRO
    GROUP_SMALL_RES = IE_WIN7 | CH_WIN10

    @functools.lru_cache()
    def _group(self):
        Result = namedtuple('Result', ['name', 'value'])
        for name, member in self._member_map_.items():
            if 'GROUP' in name and self._value_ & member.value:
                return Result(name, member.value)
        raise Exception()

    @property
    def group_value(self):
        return self._group().value

    @property
    def group_name(self):
        return self._group().name


# Desktop browsers with different OS and the 3 most popular resolutions
def browser_ff_win10(**kwargs):
    return {'browser': 'Firefox', 'os': 'Windows', 'os_version': '10', 'resolution': '1920x1080'}


def browser_ff_win7(**kwargs):
    return {'browser': 'Firefox', 'os': 'Windows', 'os_version': '7', 'resolution': '1680x1050'}


def browser_ff56_win7(**kwargs):
    return {'browser': 'Firefox', 'os': 'Windows', 'os_version': '7', 'browser_version': '56.0',
            'resolution': '1680x1050'}


def browser_ch_win10(**kwargs):
    return {'browser': 'Chrome', 'os': 'Windows', 'os_version': '10', 'resolution': '1366x768'}


def browser_ch_win7(**kwargs):
    return {'browser': 'Chrome', 'os': 'Windows', 'os_version': '7', 'resolution': '1920x1080'}


def browser_ed_win10(**kwargs):
    return {'browser': 'Edge', 'browser_version': '17.0', 'os': 'Windows', 'os_version': '10',
            'resolution': '1680x1050', 'ensureCleanSession': True}


def browser_ie_win7(**kwargs):
    return {'browser': 'IE', 'browser_version': '11.0', 'os': 'Windows', 'os_version': '7', 'resolution': '1366x768',
            'ie.ensureCleanSession': True}


# Mobile Devices
def orientation(func):
    """device orientation, h for horizontal Only works with Android devices! """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        res: dict = func(*args, **kwargs)
        if DeviceOrientation(kwargs.get('orient')) == DeviceOrientation.LANDSCAPE:
            res.update({'deviceOrientation': 'landscape'})
        return res
    return wrapper


@orientation
def device_iphone8(**kwargs):
    return {'realMobile': True, 'device': 'iPhone 8', 'os_version': '11.0', 'browserstack.safari.enablePopups': True,
            'nativeWebTap': True}  # nativeWebTap needed to open new tabs in iOS


@orientation
def device_iphonese(**kwargs):
    return {'realMobile': True, 'device': 'iPhone SE', 'os_version': '11.2', 'browserstack.safari.enablePopups': True,
            'nativeWebTap': True}  # nativeWebTap needed to open new tabs in iOS


@orientation
def device_samsung7(**kwargs):
    return {'realMobile': True, 'device': 'Samsung Galaxy S7', 'os_version': '6.0'}


@orientation
def device_samsung8(**kwargs):
    return {'realMobile': True, 'device': 'Samsung Galaxy S8', 'os_version': '7.0'}


# Tablet Devices
@orientation
def device_ipad5(**kwargs):
    return {'realMobile': True, 'device': 'iPad 5th', 'os_version': '11.0', 'browserstack.safari.enablePopups': True,
            'nativeWebTap': True}  # nativeWebTap needed to open new tabs in iOS


@orientation
def device_ipadpro(**kwargs):
    return {'realMobile': True, 'device': 'iPad Pro', 'os_version': '11.2', 'browserstack.safari.enablePopups': True,
            'nativeWebTap': True}  # nativeWebTap needed to open new tabs in iOS


def _portaltest_env():
    env = Env()
    return env
_env = _portaltest_env()


def default_browser(browser, orient):
    return {'ff_win10': browser_ff_win10,
            'ff_win7': browser_ff_win7,
            'ff56_win7': browser_ff56_win7,
            'ch_win10': browser_ch_win10,
            'ch_win7': browser_ch_win7,
            'ed_win10': browser_ed_win10,
            'ie_win7': browser_ie_win7,
            'iphone8': device_iphone8,
            'iphonese': device_iphonese,
            'samsung7': device_samsung7,
            'samsung8': device_samsung8,
            'ipad5': device_ipad5,
            'ipadpro': device_ipadpro
            }\
        .get(browser, 'ff_win10')(orient=orient)
