# -*- coding: utf-8 -*-
import pytest
import requests
import logging
from typing import Dict
from environs import Env
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from pytest_portaltest.utils import BrowserGroup, default_browser


# env for Browserstack login
env = Env()
env.read_env()

BROWSERSTACK_USERNAME = env('BROWSERSTACK_USERNAME')
BROWSERSTACK_ACCESS_KEY = env('BROWSERSTACK_ACCESS_KEY')


@pytest.fixture(scope='package')
def browser(request):
    browser = request.config.getoption('browser')
    return browser.upper()


@pytest.fixture(scope='package')
def browser_group(request):
    browser = request.config.getoption('browser')
    return BrowserGroup[browser.upper()].group_value


@pytest.fixture(scope='session')
def desired_capabilities(request):
    bs_localrun = request.config.getoption('bs_localrun')
    bs_localident = request.config.getoption('bs_localident')
    name = request.config.getoption('name', '')

    project = request.config.getoption('project')
    environment = request.config.getoption('env')

    desired_cap = {
        'name': f'{name}',
        'project': f'{project}',
        'build': f'{environment}',
        'browserstack.timezone': 'GMT-1',
        'browserstack.debug': True,
        'browserstack.local': bs_localrun,
        'acceptSslCerts': True
    }

    if bs_localident:
        desired_cap.update({'browserstack.localIdentifier': bs_localident})

    return desired_cap


@pytest.fixture(scope='package')
def driver(request, desired_capabilities) -> WebDriver:
    """Config for Browserstack and test, and logging of 'test running'"""
    browser = default_browser(request.config.getoption('browser'),
                              request.config.getoption('orient'))

    driver = webdriver.Remote(
        command_executor=request.config.getoption('bs_server'),
        desired_capabilities={**desired_capabilities, **browser})  # updates with browser_default info

    def fin():
        driver.quit()
        url = f'https://api.browserstack.com/automate/sessions/{driver.session_id}.json'
        r = requests.get(url, auth=(BROWSERSTACK_USERNAME, BROWSERSTACK_ACCESS_KEY))
        logging.info(f"Browser-URL: {r.json()['automation_session']['browser_url']}")
    request.addfinalizer(fin)
    return driver


@pytest.fixture(scope='package')
def portal_urls() -> Dict[str, str]:
    logging.warning('fixture "portal_urls" is empty!')
    return {}


@pytest.fixture(scope='package')
def portal_url(request, portal_urls):
    e = request.config.getoption('env')
    try:
        for k, v in portal_urls.items():
            if k.upper() == e.upper():
                return v
    except KeyError:
        raise Exception(f'env "{e}" not found in portal_urls')
