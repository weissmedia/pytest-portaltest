from pytest_portaltest.plugin import pytest_addoption, addoption
from pytest_portaltest.fixture import (driver,
                                       desired_capabilities,
                                       browser_group,
                                       browser,
                                       env as portaltest_env,
                                       portal_url)

